import Artist from "../contract.cdc"

pub fun main(address: Address): [Artist.Canvas] {
  let account = getAccount(address)
  let pictureReceiverRef = account
    .getCapability<&{Artist.PictureReceiver}>(/public/ArtistPictureReceiver)
    .borrow()
    ?? panic("Couldn't borrow Picture Receiver reference.")

  return pictureReceiverRef.getCanvases()
}
import Artist from "../contract.cdc"

transaction() {
  prepare(account: AuthAccount) {
    account.save<@Artist.Collection>(
      <- Artist.createCollection(),
      to: /storage/ArtistPictureCollection
    )
    account.link<&{Artist.PictureReceiver}>(
      /public/ArtistPictureReceiver,
      target: /storage/ArtistPictureCollection
    )
  }
}
import Artist from "../contract.cdc"

transaction() {
  prepare(account: AuthAccount) {
    account.unlink(/public/ArtistPictureReceiver)
    let collection <- account.load<@Artist.Collection>(
      from: /storage/ArtistPictureCollection
    )
    destroy collection
  }
}
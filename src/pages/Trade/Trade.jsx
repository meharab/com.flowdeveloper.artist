import {useContext, useReducer, useEffect, useCallback} from 'react';
import {Link} from 'react-router-dom';
import classNames from 'classnames';

import Pixel from '../../model/Pixel.js';
import Picture from '../../model/Picture.js';

import FlowContext from '../../context/Flow.jsx';

import Frame from '../Draw/Frame.jsx';

const constants = {
  flowFormat: new Intl.NumberFormat('en-US', {
    minimumFractionDigits: 4
  })
};

function reducer(state, action) {
  switch (action.type) {
    case 'setListings': {
      return {
        ...state,
        listings: action.payload.map((listing) => ({
          ...listing,
          picture: new Picture(listing.canvas.pixels, listing.canvas.width, listing.canvas.height),
          price: Number.parseFloat(listing.price)
        }))
      };
    }
    case 'setRecords': {
      return {
        ...state,
        records: action.payload.map((record) => ({
          ...record,
          picture: new Picture(record.canvas.pixels, record.canvas.width, record.canvas.height),
          seller: record.seller,
          buyer: record.buyer,
          price: Number.parseFloat(record.price)
        }))
      };
    }
    case 'startProcessing': {
      return {
        ...state,
        processingListingIndex: action.payload
      };
    }
    case 'stopProcessing': {
      return {
        ...state,
        processingListingIndex: null
      };
    }
    default:
      return state;
  }
}

function Trade(props) {
  const flow = useContext(FlowContext);
  const [state, dispatch] = useReducer(reducer, {
    listings: null,
    records: null,
    processingListingIndex: null
  });

  const fetchMarketData = useCallback(
    async () => {
      const listings = await flow.fetchListings();
      dispatch({type: 'setListings', payload: listings});

      const records = await flow.fetchRecords();
      dispatch({type: 'setRecords', payload: records});

      await flow.fetchBalance();
    },
    [flow]
  );

  useEffect(() => {
    fetchMarketData();
  }, [fetchMarketData]);

  const onBuy = async (listingIndex) => {
    dispatch({type: 'startProcessing', payload: listingIndex});
    await flow.buy(listingIndex);
    await flow.fetchCollection();
    dispatch({type: 'stopProcessing'});
    await fetchMarketData();
  };
  const onWithdraw = async (listingIndex) => {
    dispatch({type: 'startProcessing', payload: listingIndex});
    await flow.withdrawListing(listingIndex);
    await flow.fetchCollection();
    dispatch({type: 'stopProcessing'});
    await fetchMarketData();
  };

  return (
    <div>
      <h5 className="title is-5">Open Listings</h5>
      <table className="table is-fullwidth is-striped is-narrow">
        <thead>
          <tr>
            <th>Picture</th>
            <th>Seller</th>
            <th className="is-justify-content-flex-end has-text-right">Price</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {state.listings === null &&
            <tr>
              <td colSpan={4}>
                Loading...
              </td>
            </tr>
          }
          {state.listings && state.listings.length === 0 &&
            <tr>
              <td colSpan={4}>
                No listings found.
              </td>
            </tr>
          }
          {state.listings && state.listings.map((listing, index) => {
            const isUserOwner = flow.state.user.addr === listing.seller;

            return (
              <tr key={listing.picture.pixels}>
                <td>
                  <Frame
                    pixel={Pixel.thumbnail}
                    picture={listing.picture}
                  />
                </td>
                <td>
                  <Link to={`/account/${listing.seller}`}>{listing.seller}</Link>
                </td>
                <td className="is-justify-content-end">
                  <div className="tags has-addons are-medium is-justify-content-flex-end">
                    <span className="tag is-primary is-light">FLOW</span>
                    <span className="tag">
                      {constants.flowFormat.format(listing.price)}
                    </span>
                  </div>
                </td>
                <td>
                  {!isUserOwner &&
                    <button
                      className={classNames({
                        'button is-success': true,
                        'is-loading': state.processingListingIndex === index
                      })}
                      onClick={() => onBuy(index)}
                    >
                      Buy
                    </button>
                  }
                  {isUserOwner &&
                    <button
                      className={classNames({
                        'button is-warning': true,
                        'is-loading': state.processingListingIndex === index
                      })}
                      onClick={() => onWithdraw(index)}
                    >
                      Withdraw
                    </button>
                  }
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <h5 className="title is-5">Past Sales</h5>
      <table className="table is-fullwidth is-striped is-narrow">
        <thead>
          <tr>
            <th>Picture</th>
            <th>Seller</th>
            <th>Buyer</th>
            <th className="is-justify-content-flex-end has-text-right">Price</th>
          </tr>
        </thead>
        <tbody>
          {state.records === null &&
            <tr>
              <td colSpan={4}>
                Loading...
              </td>
            </tr>
          }
          {state.records && state.records.length === 0 &&
            <tr>
              <td colSpan={4}>
                No records found.
              </td>
            </tr>
          }
          {state.records && state.records.map((record, index) => {
            return (
              <tr key={index}>
                <td>
                  <Frame
                    pixel={Pixel.thumbnail}
                    picture={record.picture}
                  />
                </td>
                <td>
                  <Link to={`/account/${record.seller}`}>{record.seller}</Link>
                </td>
                <td>
                  <Link to={`/account/${record.buyer}`}>{record.buyer}</Link>
                </td>
                <td className="is-justify-content-end">
                  <div className="tags has-addons are-medium is-justify-content-flex-end">
                    <span className="tag is-primary is-light">FLOW</span>
                    <span className="tag">
                      {constants.flowFormat.format(record.price)}
                    </span>
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default Trade;